# Volets Domotique

## Initialisation de la Raspberry
  * Télécharger la dernière version de Raspbian
  * Décompressez l’archive
  * Téléchargez Win32DiskImager
  * Insérez la carte SD dans l’ordinateur, formater la et lancez Win32DiskImager
  * Flasher la carte SD avec Raspbian
---

## Configurer Raspbian
  * Pour passer votre clavier en AZERTY sur Raspbian, tapez `sudo raspi-config` dans le terminal, puis dans les menus, rendez-vous dans : « Localisation Options » / « Change keyboard layout », choisissez « Français » avec une disposition « Par défaut » et validez.
  * Pour activer le SSH sur Raspbian, tapez `sudo raspi-config` dans le terminal, puis dans les menus, rendez-vous dans : « Configurer les connexions des périphériques » / « SSH », activez et validez.
  * Connectez le Raspberry au réseau.
  * Puis mettre le Raspberry à jour avec 'sudo apt-get update' et 'sudo apt-get upgrade'
  * Reboot le Raspberry `sudo reboot`
---

## Connection SSH
  * Sur le terminal du Raspberry trouvez son ip avec `ifconfig`
  * Sur un ordinateur lancez PuTTY et connectez-vous en ssh sur la Raspberry  	*Identifiant : pi Mot de passe : raspberry* 

## Installation de Apache2, PHP et WiringPi Library
   * Installation de Apache2 `sudo apt-get install apache2`
   * Installation de PHP `sudo apt-get install php`
   * Installation de WiringPi `sudo apt-get install wiringpi`

## Création de l'interface WEB
  * Dans `/var/www/html/` retirer index.html avec `sudo rm index.html`
  * Dans `/var/www/html/` placer le fichier index.php du git

## Cablage de la Raspberry
   * Cabler comme sur le shemat

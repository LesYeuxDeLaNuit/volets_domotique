<html>
  <head>
    <meta name="viewport" content="width=device-width" />
    <title>Domotique Server</title>
  </head>
  <body>
    <center>
      <h1>Control by Webserver</h1>
      <form method="get" action="index.php">
        <input type="submit" style = "font-size: 14 pt" value="Open" name="op">
        <input type="submit" style = "font-size: 14 pt" value="Stop" name="stop">
        <input type="submit" style = "font-size: 14 pt" value="Close" name="cl">
      </form>
    </center>
  <?php

    shell_exec('gpio -g mode 27 out');
    shell_exec('gpio -g mode 22 out');

    if(isset($_GET['cl'])){
      echo "Close";
      shell_exec('gpio -g write 27 0');
      shell_exec('gpio -g write 22 1');
    }
    else if(isset($_GET['op'])){
      echo "Open";
      shell_exec('gpio -g write 22 0');
      shell_exec('gpio -g write 27 1');
    }
    else if(isset($_GET['stop'])){
    {
      echo "Stop";
      shell_exec('gpio -g write 27 0');
      shell_exec('gpio -g write 22 0');
    }
  ?>
  </body>
</html>
